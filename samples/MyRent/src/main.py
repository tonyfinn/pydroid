from random import randint
import json

from activities import PortfolioActivity, ResidenceActivity

from pydroid.app import PyDroidApp

portfolio = []


app = PyDroidApp('com.tonyfinn.pydroid.myrent')
app.register_activity(PortfolioActivity, default=True)
app.register_activity(ResidenceActivity)
app.start()

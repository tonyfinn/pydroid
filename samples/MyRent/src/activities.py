__author__ = 'tony'

from pydroid.activity import PythonActivity
from pydroid.ui import Button, TextView, EditText, CheckBox, LinearLayout, CollectionAdapter, ListView
from pydroid.utils import Intent
from pydroid.providers import Contacts
from models import Residence


class PortfolioActivity(PythonActivity):
    def __init__(self, activity):
        super(PortfolioActivity, self).__init__(activity)
        self.on('create', self.setup_views)
        self.on('pause', self.save_portfolio)
        #self.title = 'Portfolio'

    def setup_views(self, saved_state):
        self.load_portfolio()
        adapter = CollectionAdapter(self._context, portfolio, True)
        self.lv = ListView(self._context)
        self.lv.adapter = adapter
        self.lv.on('itemclick', self.open_residence)
        self.content = self.lv
        self.set_menu('residencelist')
        self.on_menu_item('menu_item_new_residence', self.new_residence)
        self.on('resume', self.refresh_list)

    def open_residence(self, target, position, itemid):
        self.launch_own_activity('ResidenceActivity', residence=position)

    def new_residence(self):
        portfolio.append(Residence('Location'))
        self.launch_own_activity('ResidenceActivity', residence=len(portfolio)-1)

    def refresh_list(self):
        self.lv.notify_changed()

    def load_portfolio(self):
        global portfolio
        try:
            print('Loading old data')
            with open('{}/portfolio.json', 'r') as input_file:
                raw_file = json.load(input_file)
                for entry in raw_file:
                    portfolio.append(Residence.from_dict(entry))
        except IOError:
            print('Loading sample data')
            for i in range(10):
                geolocation = randint(-180, 180), randint(-180, 180)
                portfolio.append(Residence(geolocation))

    def save_portfolio(self):
        print('Saving portfolio')
        print('Destination: {}'.format(self.files_dir))

        output = []
        for residence in portfolio:
            output.append(residence.to_dict())
        with open('{}/portfolio.json'.format(self.files_dir), 'w') as output_file:
            json.dump(output, output_file)


class ResidenceActivity(PythonActivity):
    PICK_TENANT_ID = 1

    def __init__(self, activity):
        super(ResidenceActivity, self).__init__(activity)
        self.residence_pos = None
        self.on('create', self.setup_views)
        self.on('activityresult', self.tenant_picked)
        #self.title = 'Residence'

    def setup_views(self, *args):
        self.load_layout('residence')
        self.residence_pos = int(self.intent.getStringExtra('residence'))
        date_button = self.find('dateButton')
        date_button.enabled = False
        date_button.text = portfolio[self.residence_pos].date_string

        rented_check = self.find('rentedCheck')
        rented_check.checked = portfolio[self.residence_pos].rented
        rented_check.on('checkchange', self.rented_changed)

        self.geolocation = self.find('locationEditText')
        self.geolocation.text = '{}, {}'.format(*portfolio[self.residence_pos].geolocation)
        self.geolocation.on('aftertextchanged', self.geolocation_changed)

        self.tenant_button = self.find('tenant')
        self.tenant_button.on('click', self.pick_tenant)

        self.send_report_button = self.find('residenceReportButton')
        self.send_report_button.on('click', self.send_report)

    def rented_changed(self, cb, new_state):
        portfolio[self.residence_pos].rented = new_state
        print('Rent status changed to {}'.format(new_state))

    def geolocation_changed(self, *args):
        parts = self.geolocation.text.split(',')
        coords = (float(parts[0]), float(parts[1]))
        portfolio[self.residence_pos].geolocation = coords
        print('Geolocation changed to {}'.format(coords))

    def pick_tenant(self, view):
        Contacts.show_contact_picker(self, self.PICK_TENANT_ID)

    def tenant_picked(self, request_code, response_code, data):
        print(data.getData())
        print(Contacts.contact_from_uri(self, data.getData()).name)

    def send_report(self):
        intent = Intent(Intent.ACTION_SENDTO, data_uri='mailto:///')
        intent[Intent.EXTRA_SUBJECT] = "Residence Report"
        intent[Intent.EXTRA_TEXT] = "Sample Residence Report"
        self.start_activity(intent)


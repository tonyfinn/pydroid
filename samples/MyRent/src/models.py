from datetime import datetime


class Residence(object):
    nextId = 1

    def __init__(self, geolocation, date=None, rented=False, id=None):
        if id:
            self.id = id
            if id >= self.nextId:
                self.nextId = id + 1
        else:
            self.id = self.nextId
            self.nextId += 1
        if date:
            self.date = date
        else:
            self.date = datetime.now()
        self.rented = rented
        self.geolocation = geolocation

    @property
    def date_string(self):
        return 'Registered: {:%c}'.format(self.date)

    def __str__(self):
        return '{}, {}\n{}'.format(self.geolocation[0], self.geolocation[1], self.date_string)

    def to_dict(self):
        return {
            'id': self.id,
            'date': self.date.ctime(),
            'rented': self.rented,
            'geolocation': self.geolocation
        }

    @classmethod
    def from_dict(self, dct):
        return Residence(tuple(dct['geolocation']), datetime.strptime(dct['date']), dct['rented'], dct['id'])

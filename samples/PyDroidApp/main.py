from pydroid import PyDroidApp
from pydroid.activity import PythonActivity
from pydroid.ui import Button, TextView
from android.runnable import run_on_ui_thread

class HomeActivity(PythonActivity):
    def __init__(self, activity):
        super(HomeActivity, self).__init__(activity)
        self.on('create', self.add_button)
        
    @run_on_ui_thread
    def add_button(self, *args):
        button = Button(self.java_activity, text='Created From Python', click=self.button_clicked)
        self.content = button

    @run_on_ui_thread
    def button_clicked(self, *args):
        self.content = TextView(self.java_activity, text='Button has been clicked')

app = PyDroidApp()
app.register_activity(HomeActivity, default=True)
app.start()

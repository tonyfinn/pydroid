"""
Tests for jnius mock.
"""

__author__ = 'tony'

import pydroid
pydroid.use_mock_jnius = True
from pydroid import jnius
from pydroid.jnius import PythonJavaClass, java_method, autoclass, register_java_mock, java_mock


class FakeJavaApi(object):
    def __init__(self, id):
        self._id = id

    def getId(self):
        return self._id

    def setId(self, id):
        self._id = id


def test_jnius_is_mocked():
    assert jnius.is_mock_jnius, "Application failed to load mock PyJNIus instead of real PyJNIus"


def test_jnius_create_class():
    class SomeClass(PythonJavaClass):
        __javainterfaces__ = ['java.lang.Iterable']

        def __init__(self):
            pass

        @java_method('()Ljava/lang/Object;', name='getItem')
        def __getitem__(self, item):
            return 1

    x = SomeClass()
    assert x['item'] == 1, "PythonJavaClass mock interferes with class operation"


def test_autoclass_no_predefined_mock():
    JavaString = autoclass('java.lang.String')
    x = JavaString('hello world')

    assert x.__javaclass__ == 'java.lang.String', 'autoclass failed to set __javaclass__ on Java class'


def test_autoclass_no_predefined_mock_no_duplicates():
    JavaString = autoclass('java.lang.String')
    JavaString2 = autoclass('java.lang.String')
    assert JavaString is JavaString2


def test_autoclass_predefined_mock():
    register_java_mock('com.someapp.ThingWithId', FakeJavaApi)

    ThingWithId = autoclass('com.someapp.ThingWithId')
    y = ThingWithId(5)
    assert y.getId() == 5, "User provided implementation was not used."


def test_java_mock_decorator():
    @java_mock('java.util.HashMap')
    class HashMapMock(object):
        pass

    HashMap = autoclass('java.util.HashMap')
    assert HashMap is HashMapMock, 'Decorated class not used for Java mocking.'

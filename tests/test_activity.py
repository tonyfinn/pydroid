__author__ = 'tony'

import pydroid
pydroid.use_mock_jnius = True

from pydroid.activity import PythonActivity
from pydroid.utils import Bundle
from pydroid.test import PyDroidTestApp
from pydroid.jnius import autoclass

from mock import MagicMock


class MyActivity(PythonActivity):
    def __init__(self, java_activity, app):
        super(MyActivity, self).__init__(java_activity, app)


def start_app_with_activity(activity_class):
    app = PyDroidTestApp('com.example.pydroid')
    app.register_activity(activity_class, default=True)
    app.start()
    return app


def test_activity_registers_when_launched():
    app = start_app_with_activity(MyActivity)

    assert type(app.current_activity) is MyActivity


def test_activity_handles_event():
    app = start_app_with_activity(MyActivity)
    param = object()
    handler = MagicMock()
    handler2 = MagicMock()
    activity_instance = app.current_activity
    activity_instance.on('create', handler)
    activity_instance.on('create', handler2)
    activity_instance.fire('create', param)
    assert handler.called, "No Event Handler called."
    assert handler2.called, "Only first event handler called"


def test_on_create_fires_event():
    app = start_app_with_activity(MyActivity)
    handler = MagicMock()
    saved_instance_state = autoclass('android.content.Bundle')()
    activity_instance = app.current_activity
    activity_instance.on('create', handler)
    activity_instance.on_create(saved_instance_state)
    assert handler.called, "Did not call create handler for on_create"


def test_on_pause_fires_event():
    app = start_app_with_activity(MyActivity)
    handler = MagicMock()
    saved_instance_state = autoclass('android.content.Bundle')()
    activity_instance = app.current_activity
    activity_instance.on('pause', handler)
    activity_instance.on_pause()
    assert handler.called, "Did not call pause handler for on_pause"


def test_on_start_fires_event():
    app = start_app_with_activity(MyActivity)
    handler = MagicMock()
    activity_instance = app.current_activity
    activity_instance.on('start', handler)
    activity_instance.on_start()
    assert handler.called, "Did not call start handler for on_start"


def test_on_resume_fires_event():
    app = start_app_with_activity(MyActivity)
    handler = MagicMock()
    activity_instance = app.current_activity
    activity_instance.on('resume', handler)
    activity_instance.on_resume()
    assert handler.called, "Did not call resume handler for on_resume"


def test_on_create_passes_wrapped_bundle():
    app = start_app_with_activity(MyActivity)
    handler = MagicMock()
    saved_instance_state = autoclass('android.content.Bundle')()
    activity_instance = app.current_activity
    activity_instance.on('create', handler)
    activity_instance.on_create(saved_instance_state)
    first_param = handler.call_args[0][0]
    assert type(first_param) is Bundle
    assert first_param.native_bundle == saved_instance_state

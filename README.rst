PyDroid
=======

PyDroid is a toolset and library for developing Android applications using Python. It allows access to the native platform features to a much greater degree than alternatives. This is most notable in its ability to create a standard application UI using the SDK provided widgets, but extends to a variety of areas, such as the ability to launch other application's activities if they're listening for intents.

Components
----------

This repository contains a number of components developed for this project. The major parts are:

* docs

  * These are the RST source files for the end user documentation. This documentation is compiled by Sphinx into HTML files and is available at http://pydroid.tonyfinn.com

* pydroid

  * This is the pydroid library itself. Developers creating an application will write their code against this library and use it to interact with the Android SDK.

* samples/MyRent    

  * This is the sample application which illustrates use of the library and is also used as a sanity test for builds of the library.

* tests

  * These are the tests for the pydroid library itself. They can be ran by the use of py.test.

* gradle-plugin 

  * The Gradle plugin is used for building an application written using this toolset into an APK that can then be used on an Android device.


In addition, there are modified versions of two libraries from the Kivy project which are used to support this in other repositories.

* `Python For Android <https://bitbucket.org/tonyfinn/python-for-android>`_

  * This is a slimmed down version of Kivy's Python for Android to remove heavy dependencies like SDL and to introduce required support for the PyDroid library.

* `PyJNIus <https://bitbucket.org/tonyfinn/pyjnius-sdlless>`_

  * This is a fork of PyJNIus to remove the SDL dependency.

Building & Running
===================

* Docs

  * ``pip install Sphinx``
  * Run ``make html`` in the docs directory.

* Tests

  * ``pip install pytest``
  * Run ``py.test tests`` in the root directory.

* Gradle Plugin

  * Install Gradle
  * Run ``gradle install`` in the gradle-plugin directory.

* PyDroid library

  * ``python setup.py install`` in the root directory.
  * You do not need to manually install this if using the Gradle plugin.

* MyRent Sample

  * Install the Gradle plugin
  * Install the Android SDK & NDK
  * Run ``gradle assembleDebug`` in the samples/MyRent folder.
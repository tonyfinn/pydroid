package com.tonyfinn.pydroid

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.Copy
import org.gradle.api.tasks.Exec
import org.gradle.api.tasks.bundling.Tar

class PyDroidPlugin implements Plugin<Project> {

    String getApkPath(Project project) {
        def apk_name = project.rootDir.absolutePath.replace(" ", "") +
                "-${project.pydroid.appVersion}-debug.apk"

        def path = "${project.buildDir}/pydroid/pydroid-p4a/dist/auto/bin/$apk_name"
        return path
    }

    Map<String, String> getEnvVars(Project project) {
        Properties properties = new Properties()
        properties.load(project.rootProject.file('local.properties').newDataInputStream())

        def ndkDir = properties.getProperty('ndk.dir')
        def ndkVer = properties.getProperty('ndk.ver') ?: 'r10c'
        def sdkDir = properties.getProperty('sdk.dir')
        def sdkVer = properties.getProperty('sdk.ver') ?: '19'

        return [ANDROIDNDK: ndkDir, ANDROIDNDKVER: ndkVer, ANDROIDSDK: sdkDir, ANDROIDAPI: sdkVer]
    }

    void apply(Project project) {

        def tempDir = "${project.buildDir}/pydroid"
        def p4aDir = "${tempDir}/pydroid-p4a"
        def distDir = "${p4aDir}/dist/auto"
        project.extensions.create("pydroid", PyDroidPluginExtension)
        project.configure(project) {
            apply plugin: 'android'

            dependencies {
                compile "com.android.support:support-v4:18.0.+"
                compile "com.android.support:appcompat-v7:20.+"
            }

            android {
                compileSdkVersion 19
                buildToolsVersion "21.1.2"
                defaultConfig {
                    applicationId project.pydroid.appPackage
                    minSdkVersion 13
                    targetSdkVersion project.properties['sdk.ver']
                    versionCode 1
                    versionName "1.0"
                }
                sourceSets.main {
                    def srcDir = "${distDir}/src"
                    manifest.srcFile "${distDir}/AndroidManifest.xml"
                    java.srcDirs = [srcDir]
                    jniLibs.srcDir "${distDir}/libs"
                    res.srcDirs = ["${distDir}/res"]
                    resources.srcDirs = [srcDir]
                    assets.srcDirs = ["${distDir}/assets"]
                    renderscript.srcDirs = [srcDir]
                    aidl.srcDirs = [srcDir]
                }
            }
        }

        project.task("hostpython") {
            def pyVersion = "2.7.2"
            def pyDir = new File(tempDir, "Python-$pyVersion")
            outputs.dir(pyDir)

            doLast {
                pyDir.mkdirs();
                ant.get(src: "http://python.org/ftp/python/$pyVersion/Python-${pyVersion}.tar.bz2", dest: "$tempDir/python-${pyVersion}.tar.bz2")
                ant.untar(src: "$tempDir/python-${pyVersion}.tar.bz2", compression: "bzip2", dest: tempDir)
            }
        }

        project.task("p4a") {
            //dependsOn project.hostpython
            ext.destDir = new File(tempDir, "pydroid-p4a")
            outputs.dir(ext.destDir)

            doLast {
                destDir.mkdirs()
                if(project.hasProperty('local_p4a')) {
                    ant.copydir(src: project.local_p4a, dest: "${tempDir}/pydroid-p4a")
                } else {
                    ant.get(src: "http://jenkins.tonyfinn.com/builds/pydroid-p4a.tar.gz", dest: "${tempDir}/python-for-android.tar.gz")
                    ant.untar(src: "${tempDir}/python-for-android.tar.gz", dest: "${tempDir}", compression: "gzip")
                }
                ant.chmod(perm: "u+x") {
                    fileset(dir: p4aDir) {
                        include(name: '**/*.sh')
                        include(name: 'src/tools/*')
                    }
                }
            }
        }

        project.task(type: Exec, "distribution") {
            dependsOn project.p4a

            mustRunAfter "cleanDistribution", "p4a"

            outputs.dir project.file("${tempDir}/pydroid-p4a/build")

            workingDir "${tempDir}/pydroid-p4a"

            commandLine "./distribute.sh", "-C", "-m", "pydroid", "-d", "auto"

            environment getEnvVars(project)

            if(project.hasProperty("pydroid_path")) {
                environment("LOCAL_PYDROID_DIR", project.pydroid_path)
            }
        }

        project.task(dependsOn: "distribution", type: Tar, "privateTar") {
            def assetsPath = "${tempDir}/pydroid-p4a/dist/auto/assets"
            from "${tempDir}/pydroid-p4a/dist/auto/private"
            destinationDir project.file(assetsPath)
            archiveName "private.mp3"
            compression "gzip"
        }

        project.task(dependsOn: "distribution", type: Tar, "publicTar") {
            def assetsPath = "${tempDir}/pydroid-p4a/dist/auto/assets"
            def pySrcDir = "${project.rootDir}/src"
            from pySrcDir
            destinationDir project.file(assetsPath)
            archiveName "public.mp3"
            include "**.py"
            compression "gzip"
        }

        project.task(dependsOn: ["publicTar", "privateTar"], "codetar") {
        }

        project.task(dependsOn: "distribution", type: Copy, "copyResources") {
            def resDir = "${project.rootDir}/res";
            def outputDir = "${tempDir}/pydroid-p4a/dist/auto/res"
            inputs.dir project.file(resDir)
            outputs.dir project.file(outputDir)
            from resDir
            include '**/*'
            into outputDir
        }

        project.task(dependsOn: ["distribution", "codetar", "copyResources"], type: Exec, "app") {
            mustRunAfter "cleanApp"

            inputs.dir project.rootDir.absolutePath
            outputs.dir "${tempDir}/pydroid-p4a/dist/auto"

            workingDir "${tempDir}/pydroid-p4a/dist/auto"

            doFirst {
                ant.chmod(perm: 'u+x') {
                    fileset(dir: "${tempDir}/pydroid-p4a/dist/auto") {
                        include(name: '**.py')
                    }
                }
            }

            environment getEnvVars(project)

            project.afterEvaluate {
                def args = [
                        "./build.py", "--dir",
                        "${project.rootDir.absolutePath}/src",
                        "--version", project.pydroid.appVersion,
                        "--name", project.pydroid.appName,
                        "--package", project.pydroid.appPackage,
                        "--orientation", "portrait",
                        "debug"
                ]

                commandLine args
            }
        }

        project.preBuild.dependsOn project.app

        project.task(dependsOn: ["cleanDistribution",
            "cleanApp", "distribution", "app"], "rebuild") {
        }

        project.task(type: Exec, dependsOn: "app", "deploy") {
            mustRunAfter "app"
            def path = getApkPath(project)
            commandLine "adb", "install", "-r", path
        }
    }
}

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='pydroid',
    version='0.1.0',
    description='Python wrapper and toolset for developing Android SDK applications',
    url='http://bitbucket.org/tonyfinn/pydroid',
    author='Tony Finn',
    author_email='tony@tonyfinn.com',
    include_package_data=True,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
    ],
    keywords='python android',
    packages=['pydroid', 'pydroid.ui', 'pydroid.test', 'pydroid.test.jnius'],
    install_requires=[],
    extras_require={
        'local_tests': ['mock']
    }
)

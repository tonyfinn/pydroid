Getting Started
===============

PyDroid expects a standard structure for an application. At a minimum, an
application should have a ``build.gradle`` file and a ``src`` folder, containing your
Python code for your application. Inside the ``src`` folder should be your main.py
file, which is the entry point to your application. In addition, you will
likely want a ``res/`` folder if you wish to provide Android resources such as layouts
or strings for translation with your application.

Creating An Activity
--------------------

The first step in a basic application is to create a new Activity. An Activity
represents a single screen centred around a single task in an Android application.
In your Python code, you will need to make a class that inherits from
:class:`.PythonActivity`::

    from pydroid.activity import PythonActivity

    class HomeActivity(PythonActivity):
        def __init__(self, activity):
            super(HomeActivity, self).__init__(self, activity)

The constructor to your activity recieves one parameter. This parameter is a
reference to the underlying Java Activity from the Android API [#f1]_.

In order to bind to events from the Android lifecycle, you must use the
on method, for example, to create a UI element when the page is loaded ,
use the :py:meth:`.PythonActivity.on` method.
For this method you pass the name and a reference to the function or
method to handle that lifecycle event. Nearly all activities will want to
listen for the create event, which indicates when a new instance of that activity
has been created. The handler for this event will usually create the activity's
UI.

::

    from pydroid.activity import PythonActivity

    class HomeActivity(PythonActivity):
        def __init__(self, activity):
            super(HomeActivity, self).__init__(self, activity)
            self.on('create', self.setup_views)

        def setup_views(self, *args):
            self.load_layout('sample')

This will load the layout in ``res/layouts/sample.xml`` when this activity
is launched.

Initialising Your Application
-----------------------------

The other important task when starting your application is to initialise
PyDroid and provide it information about which activities are included in
your application. This is done via an instance of :class:`.PyDroidApp`. For example
to setup an application that consists only of the activity above, you might write
the following ``src/main.py`` file::

    from pydroid.app import PyDroidApp

    from .myactivities import HomeActivity

    app = PyDroidApp('com.example.myapp')
    app.register_activity(HomeActivity, default=True)
    app.start()

Building Your Application
--------------------------

For the build process, PyDroid relies on Gradle. To configure this, you must
create a ``build.gradle``. This should be created at the root of your project.

.. code-block:: groovy

    // Tell Gradle where to locate the Android and PyDroid plugins
    buildscript {
        repositories {
            jcenter()
            mavenLocal()
            maven {
                url "http://dl.bintray.com/tonyfinn/pydroid-gradle-plugin"
            }
        }
        dependencies {
            classpath 'com.tonyfinn.pydroid:pydroid-plugin:0.1.0'
            classpath 'com.android.tools.build:gradle:1.0.1'
        }
    }

    // Tell Gradle where to locate the Android support library and other dependencies
    repositories {
        jcenter()
    }

    // Enable PyDroid
    apply plugin: 'com.tonyfinn.pydroid'

    // Basic Configuration
    pydroid {
        appVersion = 1
        appPath = "."
        appPackage = 'com.tonyfinn.pydroid.myrent'
        appName = "PyDroid MyRent"
    }

.. rubric:: Footnotes

.. [#f1] If you require functionality that is not currently provided by the pydroid library, you can access the Java APIs directly through this reference. For details of calling Java code see the `PyJNIus docs <https://pyjnius.readthedocs.org/en/latest/>`_, but this should only be done when PyDroid does not provide a Python API for that functionality.

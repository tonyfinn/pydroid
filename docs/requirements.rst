Requirements
============

PyDroid requires a number of dependencies to be installed before starting
development. These are listed below:

* `Android SDK <https://developer.android.com/sdk/index.html#Other>`_

  * When installing SDK components, please ensure that at a minimum, you install
    the following components:

    * Android 4.4.2 (API 19)
    * Android SDK Tools
    * Android SDK Platform Tools
    * Android SDK Build Tools (Tested with 21.1.2, later versions should also work)
    * Android Support Repository
  * You may wish to install other API levels to test your application also.
* `Android NDK <https://developer.android.com/tools/sdk/ndk/index.html>`_ r10c or later
* `Gradle <http://gradle.org>`_ 2.2 or later
* Cython (``pip2 install cython``)
* Standard Unix tools: gcc g++ unzip make tar bzip2
    * On Linux systems, these are usually either pre-installed or can
      be installed via a package named build-essential or similar.
    * *(unsupported)* Cygwin users can install these via Cygwin
    * *(unsupported)* OS X users may be able to install these via homebrew

In addition, you most configure Gradle to tell it where the SDK and NDK tools
are located. There are two options for this. You may either place it in a project
specific ``gradle.properties`` file at the root of your project, or configure it
globally in ``GRADLE_HOME/gradle.properties``. Either way, you must insert the
following lines::

    ndk.dir=/path/to/ndk
    ndk.ver=<NDK Version>
    sdk.dir=/path/to/sdk
    sdk.ver=<SDK API Level>

Currently, PyDroid has only been tested on Linux, though it may be possible to
use under cygwin or OS X systems also. However, these platforms are unsupported.

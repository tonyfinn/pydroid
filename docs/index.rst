.. PyDroid documentation master file, created by
   sphinx-quickstart on Sun Apr  5 16:46:48 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PyDroid - Native Android UI in Python
=====================================

Contents:

.. toctree::
   :maxdepth: 2
   
   intro
   requirements
   gettingstarted
   ui
   api



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


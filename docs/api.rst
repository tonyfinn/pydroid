API
===

Application
-----------

.. autoclass:: pydroid.PyDroidApp
   :members:
   :special-members: __init__



Activity
--------

.. autoclass:: pydroid.activity.PythonActivity
   :members:


Fragments
---------

.. autoclass:: pydroid.fragment.PythonFragment
   :members:


UI
--

.. automodule:: pydroid.ui
   :members:
   :inherited-members:
   :special-members: __init__

Utilities
---------

.. automodule:: pydroid.utils
   :members:
   :special-members:

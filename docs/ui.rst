UI
====

.. toctree::

There are two ways to create UI elements using the PyDroid library. The first method,
which is recommended in most cases is to create an XML layout file. The format for these XML files
are the same as for any other Android application, and as such documentation for how to
create them can be found in the
`Android documentation <https://developer.android.com/training/index.html>`_. One important
caveat to keep in mind is that you cannot assign event listeners in your XML code while using
PyDroid. Rather than attributes like android:onClick, you should set up your event listeners in
code.

The second method is to create your UI elements at runtime in code. This should only be done where
it is not possible to make use of an XML layout.

Using XML Files
---------------

XML files can be created either manually or via any standard Android GUI editor, such
as those built into IntelliJ's Android plugin or Eclipse ADT. The layouts should be placed in
the ``res/layout/`` folder of your application. An activity can then load a layout at runtime
by using the :meth:`.PythonActivity.load_layout` method. This should be called with the name of the
XML file to use::

    class MyActivity(PythonActivity):
        # Various other methods
        def setup_ui(self):
            self.load_layout('mylayout')

To get a reference to a UI element declared in your layout, you can then use the
:meth:`.PythonActivity.find` method. This should take the ID defined in your XML file as a
parameter and will give you back a subclass of the :class:`.View` class corresponding to the UI
element.

.. code-block:: xml

    <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="@string/location"
            android:id="@+id/locationText">

::

    def setup_ui(self):
        text_output = self.find('locationText')
        text_output.text = 'Hello World'

Each type of UI element will have a number of properties that can be set on it specific to the
type of widget it represents.

Creating UI Elements at Runtime
-------------------------------

As well as loading


.. _wrapping-java-elements:

Wrapping Elements returned from Java APIs
-----------------------------------------

In some cases, you may wish to interact with a Java API directly, perhaps because the functionality
is not yet provided by the PyDroid library. In this case, you may receive a reference to an instance
of a Java class, with the Java API available rather than the PyDroid API.

For this case, most PyDroid UI elements accept an optional ``raw_view`` argument when they are being
created. You may use provide this with an instance of a Java class representing a UI element to
receive a wrapped version of this element.

::

    from jnius import autoclass
    from pydroid.ui import TextView

    utility = autoclass('android.some.class')
    JavaString = autoclass('java.lang.String')

    raw_text_view = utility.somethingThatReturnsATextView()

    # This is ugly..
    raw_text_view.setText(JavaString('Set from Python via JNI'))

    # Instead you can do this:
    wrapped_text_view = TextView(context, raw_view=raw_text_view)
    wrapped_text_view.text = 'Set From Python'
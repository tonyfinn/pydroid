Intro
=====

PyDroid is a library and toolset to enable application developers to build Android applications in Python with access to native platform components and integration with standard Android system features.

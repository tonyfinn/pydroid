from ..jnius import PythonJavaClass, java_method

from .widgets import TextView

class CollectionAdapter(PythonJavaClass):
    __javainterfaces__ = ['android/widget/ListAdapter']

    def __init__(self, context, collection, java_items=False):
        """
        Creates a new adapter to a Python collection
        for use in Android ListViews etc.

        :param collection: The collection to pull items from.
        :param java_items: Whether the items in this list have a
            representation in Java (i.e. if they all inherit from PythonJavaClass.
            This allows them to be returned from Adapter#getItem() to Java
            code.

        """
        super(CollectionAdapter, self).__init__()
        self.context = context
        self.collection = collection
        self.java_items = java_items

    @java_method('()I', name='getCount')
    def __len__(self):
        return len(self.collection)

    @java_method('(I)Ljava/lang/Object;')
    def getItem(self, index):
        """
        Java adapter impementation of getItem. Note: This does not have a
        useful value if the objects in the collection have no Java
        representation. Python code should use indexing instead.

        """
        if self.java_items:
            return self.collection[index]
        else:
            return None

    @java_method('(I)J')
    def getItemId(self, index):
        return -1

    @java_method('()Z')
    def hasStableIds(self):
        return False

    @java_method('()Z')
    def isEmpty(self):
        return len(collection) == 0

    @java_method('()I')
    def getViewTypeCount(self):
        return 1

    @java_method('(I)I')
    def getItemViewType(self, pos):
        return 1

    @java_method('(Landroid/database/DataSetObserver;)V')
    def registerDataSetObserver(self, dso):
        pass
    
    @java_method('(Landroid/database/DataSetObserver;)V')
    def unregisterDataSetObserver(self, dso):
        pass

    @java_method('()Z')
    def areAllItemsEnabled(self):
        return True
    
    @java_method('(I)Z')
    def isEnabled(self, pos):
        return True

    @java_method('(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;',
            name='getView')
    def get_native_view(self, index, convert_view, parent):
        return self.get_view(index, convert_view, parent)._view

    def get_view(self, index, convert_view, parent):
        text = str(self.collection[index])
        return TextView(self.context, text=text)

    def __getitem__(self, index):
        return self.collection[index]

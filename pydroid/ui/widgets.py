from __future__ import absolute_import, print_function

from ..jnius import autoclass, PythonJavaClass, java_method, cast

__all__ = all_widgets = [
    'EventSource',
    'MissingViewError',
    'UnsupportedViewError',
    'View',
    'LinearLayout',
    'TextView',
    'EditText',
    'Button',
    'CheckBox',
    'ListView'
]

JavaString = autoclass('java.lang.String')
LayoutParams = autoclass('android.view.ViewGroup$LayoutParams')

class_wrappers = {}


def ui_wrapper(android_class_name):
    global class_wrappers

    def wrap(cls):
        class_wrappers[android_class_name] = cls
        return cls

    return wrap


class MissingViewError(Exception):
    """
    Thrown when an attempt is made to load a view that does not exist
    within the current activity.
    """
    pass


class UnsupportedViewError(Exception):
    """
    Thrown when an attempt is made to load a view that the framework does not
    yet have support for.
    """
    pass


class EventSource(object):
    """
    This class represents a UI element that can generate events.
    """

    def __init__(self):
        self._handlers = {}
        self._native_handler_types = {}
        self._native_handler_instances = {}

    def fire(self, event_name, *args, **kwargs):
        for handler in self._handlers.get(event_name, []):
            handler(*args, **kwargs)
    
    def register_handler_type(self, cls, *event_names):
        # This should probably be at the class level, not instance level.
        for event_name in event_names:
            self._native_handler_types[event_name] = cls

    def on(self, event_name, handler):
        """
        :param str event_name: The name of the event to register a handler for.
        :param function handler: The function to be called for the event.
        """
        if event_name in self._handlers:
            self._handlers[event_name].append(handler)
        else:
            self._handlers[event_name] = [handler]
            
            cls = self._native_handler_types.get(event_name)
            if cls and not cls in self._native_handler_instances:
                self._native_handler_instances[cls] = cls(self)


class ClickListener(PythonJavaClass):
    __javainterfaces__ = ['android/view/View$OnClickListener']

    def __init__(self, target):
        super(ClickListener, self).__init__()
        self.target = target
        self.target._view.setOnClickListener(self)

    @java_method('(Landroid/view/View;)V', name='onClick')
    def on_click(self, view):
        self.target.fire('click', view)


class ItemClickListener(PythonJavaClass):
    __javainterfaces__ = ['android/widget/AdapterView$OnItemClickListener']

    def __init__(self, target):
        super(ItemClickListener, self).__init__()
        self.target = target
        cast('android/widget/AdapterView',
                self.target._view).setOnItemClickListener(self)

    @java_method('(Landroid/widget/AdapterView;Landroid/view/View;IJ)V',
            name='onItemClick')
    def on_item_click(self, adapter_view, clicked_item, position, itemid):
        self.target.fire('itemclick', self.target, position, itemid)


class TextChangeListener(PythonJavaClass):
    __javainterfaces__ = ['android/text/TextWatcher']

    def __init__(self, target):
        super(TextChangeListener, self).__init__()
        self.target = target
        target._tv.addTextChangedListener(self)

    @java_method('(Ljava/lang/CharSequence;III)V', name='beforeTextChanged')
    def before_text_changed(self, text, start, count, after):
        self.target.fire('beforetextchanged', text, start, count, after)

    @java_method('(Ljava/lang/CharSequence;III)V', name='onTextChanged')
    def on_text_changed(self, text, start, before, after):
        self.target.fire('textchanged', text, start, before, after)

    @java_method('(Landroid/text/Editable;)V', name='afterTextChanged')
    def after_text_changed(self, editable):
        self.target.fire('aftertextchanged', editable)


class CheckedChangedListener(PythonJavaClass):
    __javainterfaces__ = ['android/widget/CompoundButton$OnCheckedChangeListener']

    def __init__(self, target):
        super(CheckedChangedListener, self).__init__()
        self.target = target
        cast(
            'android.widget.CompoundButton',
            target._view
        ).setOnCheckedChangeListener(self) 

    @java_method('(Landroid/widget/CompoundButton;Z)V', name='onCheckedChanged')
    def on_checked_changed(self, btn, new_state):
        self.target.fire('checkchange', btn, bool(new_state))


class View(EventSource):
    """
    Represents a single UI widget in the application.

    ;;Event''

    click - handler(raw_view)

    Represents a click on this UI element.

    """

    def __init__(self, context, raw_view):
        """
        Creates a view. Library end users should rarely need
        to instantiate the View class directly.
        :param Context context: The context in which this view exists.
        :param view: Underlying Java view to wrap. This should be a reference to a view
        obtained via JNI.
        :return:
        """
        super(View, self).__init__()
        self.register_handler_type(ClickListener, 'click')
        if raw_view is None:
            raise MissingViewError
        else:
            self._view = cast('android.view.View', raw_view)
        self.context = context.native_context
        self._package = 'com.tonyfinn.pydroid.myrent'

    def layout(self, width, height):
        self._view.setLayoutParams(LayoutParams(width, height))
        return self

    @property
    def enabled(self):
        """
        This property determines whether this UI element is enabled or not.
        Reading from this property will give you the current state of the element,
        while writing to it will allow you to toggle the state.
        """
        return self._view.getEnabled()

    @enabled.setter
    def enabled(self, status):
        self._view.setEnabled(status)

    @classmethod
    def get_py_view(self, raw_view, context=None):
        raw_view_class = raw_view.getClass().getName()
        if raw_view_class in class_wrappers:
            return class_wrappers[raw_view_class](context, raw_view=raw_view)
        else:
            raise UnsupportedViewError()

    def find(self, name):
        """
        Get a reference to a child UI element of this UI element by its ID.

        See :func:`.PythonActivity.findViewById` for more infromation.

        :raises MissingViewError: If the view that you are attempting to load
            is not present in the current activity.
        :raises UnsupportedViewError: If the view is found, but no pydroid
            wrapper exists for that type of view.

        :return: A subclass of :class:`.ui.View` if an element is found,
            or None otherwise.
        """
        localRId = autoclass('{}.R$id'.format(self._package))
        element_id = getattr(localRId, name)
        raw_view = self._find(element_id)
        if not raw_view:
            raise MissingViewError()
        return View.get_py_view(raw_view, self._context)

    def _find(self, id):
        return self._view.findViewById(id)


@ui_wrapper('android.widget.LinearLayout')
class LinearLayout(View):
    """
    Layout that can handle multiple children layed out linearly. Each child element
    is layed out one after the other depending on the orientation of the LinearLayout
    """
    def __init__(self, context, children=None, raw_view=None, orientation='horizontal'):
        """

        Create a new LinearLayout

        :param context:  The platform specific context to use for this layout.
            Usually the current activity.
        :param list children:  Any default elements to add to this Linear Layout.
            This should be a list of views.
        :param raw_view: See :ref:`wrapping-java-elements`
        :param str orientation: The orientation of this view. '\'horizontal\'' results in elements
            being layed out side by side, while '\'vertical\'' results in UI elements been layed out
            one on top of another.
        """
        if not raw_view:
            AndroidLayout = autoclass('android.widget.LinearLayout')
            self._layout = AndroidLayout(context.native_context)
        else:
            self._layout = cast('android.widget.LinearLayout', raw_view)
        super(LinearLayout, self).__init__(context, self._layout)

        if orientation == 'horizontal':
            self._layout.setOrientation(AndroidLayout.HORIZONTAL)
        elif orientation == 'vertical':
            self._layout.setOrientation(AndroidLayout.VERTICAL)

        if children:
            for child in children:
                self._layout.addView(child._view)

    def add(self, element):
        """
        Add a new child element to this linear layout.

        :param View element: The element to add to this layout.
        """
        self._layout.addView(element._view)


@ui_wrapper('android.widget.TextView')
class TextView(View):
    """
    A view responsible for displaying text.
    """

    def __init__(self, context, text=None, raw_view=None, click=None):
        """
        Creates a new text view.

        :param context:
        :param text: The initial text to display in this TextView
        :param raw_view: See :ref:`wrapping-java-elements`
        :param click: A function to call when this text view is clicked on.
        """
        AndroidTextView = autoclass('android.widget.TextView')
        if not raw_view:
            self._tv = AndroidTextView(context.native_context)
        else:
            self._tv = cast('android.widget.TextView', raw_view)
        super(TextView, self).__init__(context, self._tv)
        self.register_handler_type(TextChangeListener, 
                'beforetextchanged',
                'textchanged', 
                'aftertextchanged') 

        if text:
            self.text = text
        if click:
            self.on('click', click)
    
    @property
    def text(self):
        """
        This property provides the current textual content of this element,
        and can also be written to to change the text content.
        """
        return self._tv.getText().toString()

    @text.setter
    def text(self, text):
        self._tv.setText(JavaString(text))


@ui_wrapper('android.widget.EditText')
class EditText(TextView):
    """
    A text field where the application user can enter text.
    """

    def __init__(self, context, text='', raw_view=None, click=None):
        """
        Creates a new field for editable text..

        :param context:
        :param text: The initial text to display in this TextView
        :param raw_view: See :ref:`wrapping-java-elements`
        :param click: A function to call when this element is clicked on.
        """
        AndroidEditText = autoclass('android.widget.EditText')
        if raw_view:
            self._edittext = cast('android.widget.EditText', raw_view)
        else:
            self._edittext = AndroidEditText(context.native_context)
        super(EditText, self).__init__(context, text=text, raw_view=self._edittext,
                click=click)


@ui_wrapper('android.widget.Button')
class Button(TextView):
    """
    A button which the user can click on to initiate a action.

    """

    def __init__(self, context, text='', raw_view=None, click=None):
        """
        creates a new button.

        :param context:
        :param text: the text for this button.
        :param raw_view: see :ref:`wrapping-java-elements`
        :param click: A function to call when this element is clicked on.
        """
        AndroidButton = autoclass('android.widget.Button')
        if raw_view:
            self._button = cast('android.widget.Button', raw_view)
        else:
            self._button = AndroidButton(context.native_context)
        super(Button, self).__init__(context, text=text, raw_view=self._button,
                click=click)


@ui_wrapper('android.widget.CheckBox')
class CheckBox(TextView):
    """
    A checkbox which the user cam check or uncheck.
    """

    def __init__(self, context, text='', raw_view=None, click=None, checked=False):
        """
        creates a new check box.

        :param context:
        :param text: the text for this check box.
        :param raw_view: see :ref:`wrapping-java-elements`
        :param click: A function to call when this element is clicked on.
        :param checked: The intial state of this checkbox.
        """
        AndroidCheckBox = autoclass('android.widget.CheckBox')
        if raw_view:
            self._checkbox = cast('android.widget.CheckBox', raw_view)
        else:
            self._checkbox = AndroidCheckBox(context.native_context)

        self._checkbox.setChecked(checked)

        super(CheckBox, self).__init__(context, text=text, raw_view=self._checkbox,
                click=click)

        self.register_handler_type(CheckedChangedListener, 'checkchange')

    @property
    def checked(self):
        return self._checkbox.getChecked()

    @checked.setter
    def checked(self, val):
        self._checkbox.setChecked(val)


@ui_wrapper('android.widget.ListView')
class ListView(View):

    def __init__(self, context, raw_view=None, adapter=None):
        AndroidListView = autoclass('android.widget.ListView')
        DataSetObservable = autoclass('android.database.DataSetObservable')
        self._observable = DataSetObservable()
        if raw_view:
            self._lv = cast('android.widget.ListView', raw_view)
        else:
            self._lv = AndroidListView(context.native_context)
        super(ListView, self).__init__(context, raw_view=self._lv)

        self.register_handler_type(ItemClickListener, 'itemclick')


    @property
    def adapter(self):
        return self._lv.getAdapter()

    @adapter.setter
    def adapter(self, adapter):
        self._lv.setAdapter(adapter)

    @java_method('(Landroid/database/DataSetObserver;)V')
    def register_observer(self, observer):
        self._observable.registerObserver(observer)

    @java_method('(Landroid/database/DataSetObserver;)V')
    def unregister_observer(self, observer):
        self._observable.unregisterObserver(observer)

    def notify_changed(self):
        self._observable.notifyChanged()

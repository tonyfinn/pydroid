from __future__ import print_function

import sys

from .jnius import autoclass, JavaClass, cast
from .jnius.reflect import Class

AndroidIntent = autoclass('android.content.Intent')
JavaString = autoclass('java.lang.String')
JavaBundle = autoclass('android.os.Bundle')
Uri = autoclass('android.net.Uri')
BuildVersion = autoclass('android.os.Build$VERSION')
JAVA_MAX_INT = (2**31) - 1


class InvalidContextError(Exception): 
    pass


def java_isinstance(obj, jclass):
    """
    Find out if an object represents a instance of a Java class.

    :param obj: The object to check.
    :param jclass: The Java class to figure out if the object represents a
      valid instance of. This can be one of:

      * The name of a class (e.g. 'java.lang.Class'),
      * A Python class that represents the class and extends JavaClass
      * A instance of java.lang.Class

    """

    Class = autoclass('java.lang.Class')
    if isinstance(jclass, JavaClass) and jclass.__javaclass__ == 'java/lang/Class':
        cls = jclass
    elif isinstance(jclass, str):
        cls = Class.forName(jclass)
    elif issubclass(jclass, JavaClass):
        cls = Class.forName(jclass.__javaclass__)
    else:
        return False

    if isinstance(obj, JavaClass):
        return cls.isInstance(obj)
    else:
        return False


class Context(object):
    """
    Represents a context used for allowing Android.
    """
    def __init__(self, context):
        self._context = context

    @property
    def native_context(self):
        """
        Native Android context object for passing to Java APIs.
        """
        return self._context

    @staticmethod
    def get_context(context_holder):
        """
        Utility function which retrieves either a _context value from a Python
        object which has one, or accepts a Java Context directly.

        """
        if isinstance(context_holder, JavaClass):
            if java_isinstance(context_holder, 'android.content.Context'):
                return context_holder
            else:
                raise InvalidContextError((
                    'Context is a java object that is not a '
                    + 'subclass of android.content.Context (type: {})'
                ).format(context_holder.__javaclass__))
        if hasattr(context_holder, '_context'):
            return context_holder._context
        raise InvalidContextError('Context is neither a Java object or a Python object with a _context')


class InvalidIntentError(Exception):
    pass


class Intent(object):

    ACTION_PICK = AndroidIntent.ACTION_PICK
    ACTION_SENDTO = AndroidIntent.ACTION_SENDTO
    EXTRA_SUBJECT = AndroidIntent.EXTRA_SUBJECT
    EXTRA_TEXT = AndroidIntent.EXTRA_TEXT

    def __init__(self, action=None, native_intent=None, data_uri=None, extra_data=None):
        """
        Create a new intent. You must supply at least one of action or native_intent
        :param str action: The action that this intent is looking to launch.
        :param native_intent: An existing Java Intent to wrap.
        :param data_uri: The data uri to provide to the recipient of this intent.
        :param extra_data: Any additional fields to provide.
        """

        if not extra_data:
            extra_data = []

        if action and not native_intent:
            self._intent = AndroidIntent(action)
        elif native_intent:
            self._intent = native_intent
        else:
            raise InvalidIntentError('Must specify at least action or native_intent')
        if data_uri:
            self.set_uri(data_uri)

        for key, value in extra_data:
            self[key] = value

    @property
    def data_uri(self):
        """
        The URI for the content at this intent. The meaning of this
        is defined by the recipient, and will be something like:

        contacts:///view/123
        """
        return self._intent.getData().toString()

    @data_uri.setter
    def data_uri(self, new_uri):
        if isinstance(new_uri, str):
            uri_obj = Uri.parse(new_uri)
        elif java_isinstance(new_uri, 'android.net.Uri'):
            uri_obj = new_uri
        self._intent.setData(uri_obj)

    def __getitem__(self, item):
        bundle = Bundle(self._intent.getExtras())
        return bundle[item]

    def __setitem__(self, key, value):
        if isinstance(value, str):
            self._intent.put_extra(key, JavaString(value))
        elif any(map(lambda x: isinstance(value, x), [int, long, list, bool])):
            self._intent.put_extra(key, value)


class Bundle(object):
    """
    Represents a set of key-value pairs used for serialisation
    or parameter passing in various parts of the Android API.
    """
    def __init__(self, native_bundle=None):
        """
        Create a set of serialised data that can be provided in
        intents, etc.

        :param native_bundle: An optional native bundle to use.

        """
        if native_bundle:
            self.native_bundle = native_bundle
        else:
            self.native_bundle = JavaBundle()

    def __getitem__(self, name):
        """
        Retrieve a field from a bundle.

        :param name: The name of the field.

        :return: The value stored in the bundle for that field.
        """
        if BuildVersion.SDK_INT > 21:
            # On API 21+, use the actually supported method
            # to retrieve objects of unknown type from the Bundle.
            if not self._base_bundle:
                self._base_bundle = cast('android.os.BaseBundle', self.native_bundle)
            return self._base_bundle.get(name)
        else:
            # On older APIs, read the private state of the Bundle
            # to access objects without making the end user specify
            # the Java type. This is hacky, but the only such API
            # versions this library supports (19, 20) all have the
            # required field.
            self.native_bundle.unparcel()
            return self.native_bundle.mMap.get(name)

    def __len__(self):
        return self.native_bundle.size()

    def __iter__(self):
        """
        Iterates  over the bundle, returning (name, item)
        for each item in the bundle.
        """
        for key in self.native_bundle.keySet():
            yield (key, self[key])

    def put_int(self, name, value):
        """
        Explicitly store a Python integer in an int field in the bundle.

        You should only need to call this if you are preparing a bundle
        to pass to a Java library. Otherwise, simply use bundle[name] = some_int

        :param name: The key name to use for the bundle.
        :param int value: A value

        :raises ValueError: If the value in the int is larger than allowed
            by the Java integer type. (Only a concern for a future Py3k port.
            All Python 2 ints fit in this range)
        """
        if value > JAVA_MAX_INT:
            raise ValueError('Number must be within the range of ')
        self.native_bundle.putInt(name, value)

    def put_float(self, name, value):
        """
        Explicity store a Python float in a float field in this bundle.

        This should ''only'' be used when you need to create a bundle
        for Java code that expects an item stored as a float. As Python
        floats are equivalent to Java doubles, there will be a loss of precision.

        :param str name:
        :param value:
        """
        self.native_bundle.putFloat(name, value)

    def __setitem__(self, name, item):
        """
        Store a field in a bundle. Be aware that only certain
        primitives are supported. Currently, you may store strings,
        numbers and booleans in a bundle.

        Java compatibility warning: All Python integers are stored in
        Long fields and all Python floats are stored in Double fields.
        If you must place items in float or int fields to communicate with
        Java code, use :meth:`.put_int` or  :meth:`.put_float` explicitly.

        :param str name:
        :param item:
        """
        if isinstance(item, bool):
            self.native_bundle.putBoolean(name, item)
        elif isinstance(item, int):
            self.native_bundle.putLong(name, item)
        elif sys.version_info[0] > 2 and isinstance(item, long):
            self.native_bundle.putLong(name, item)
        elif isinstance(item, basestring):
            self.native_bundle.putString(item)
        elif isinstance(item, float):
            self.native_bundle.putDouble(item)

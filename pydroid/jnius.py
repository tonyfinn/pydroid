__author__ = 'tony'

from . import use_mock_jnius
import sys

if not use_mock_jnius:
    from jnius import *
    import jnius.reflect as reflect
else:
    from .test.jnius import *
    from .test.jnius import reflect
    sys.modules['pydroid.jnius.reflect'] = reflect


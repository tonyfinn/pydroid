from .jnius import PythonJavaClass, autoclass, java_method
from .ui import View

from .app import PyDroidApp


JavaFragment = autoclass('android.app.fragment')


class FragmentAlreadyCreatedError(Exception): pass


class ViewNotYetCreatedError(Exception): pass


class PythonFragment(PythonJavaClass):
    """
    Base class for all fragments implemented in Python. Your own fragments should
    inherit from this fragment and call its __init__ method in their own.

    It has a number of events which you can listen to, based on the standard fragment
    lifecycle. In particular, you can listen to the following events:

    * ''create'' - This event occurs when the fragment itself is being created.
    * ''createview'' - This event occurs when it is time for the fragment to create its UI.
    * ''activitycreated'' - This event occurs when the fragment's acitivity is finished being created.
    * ''pause'' - This even occurs when the fragment is paused. It is possible that the fragment
        could be terminated after this method, so make sure to save any important state in this method.
    * ''resume'' - this occurs when the fragment is resumed after being paused.

    An important point to consider is that the Fragment's own activity may not yet be ready
    during the create and creativeview events and as such should not be relied upon in these
    events. Any initialisation that depends on the state of the fragment should be performed
    in a handler for the activitycreated event.
    """
    __javainterfaces__ = ['com.tonyfinn.pydroid.PythonFragment']
    __javacontext__ = 'app'

    event_types = ['create', 'pause', 'resume', 'createview', 'activitycreated']

    def __init__(self):
        super(PythonFragment, self).__init__()
        self._fragment = JavaFragment(self)
        self._context = None
        self._activity = None
        self._layout = None
        self._view_created = False
        self._handlers = {event: [] for event in self.event_types}
        self._root_view = None

    @property
    def root_view(self):
        """
        Returns the top level UI element for this Fragment. Please be aware this is
        not available until after the ''createview'' event completes.
        :return View:
        """
        return self._root_view

    def find(self, name):
        """
        Find an element within the fragment, similar to :func:`PythonActivity.find`.
        Note however that this method is not available until after the createview event completes.
        :param name:
        :return View:
        """
        return self.root_view.find(name)

    def load_layout(self, layout_name):
        """
        Sets the layout to be used for this UI element. This should be called
        at least once before the createview event occurs.

        See also: :func:`PythonActivity.load_layout`
        :param str layout_name: The name of the XML file containing the layout to use for
        this fragment.
        """
        if self._view_created:
            raise FragmentAlreadyCreatedError()
        else:
            self._layout = layout_name

    def fire(self, event, *args):
        for handler in self._handlers[event]:
            handler(*args)

    @java_method('(Landroid/os/Bundle;)V')
    def on_create(self, saved_instance_state):
        self.fire('create')

    @java_method('(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;')
    def on_create_view(self, layout_inflater, container, saved_instance_state):
        localRLayout = autoclass('{}.R$layout'.format(self._package))
        self._view_created = True
        self.fire('createview')
        self._context = layout_inflater.getContext()
        if self._layout:
            view = layout_inflater.inflate(getattr(localRLayout, self._layout), container)
        self._root_view = View.get_py_view(view, self._context)
        return view

    @java_method('(Landroid/os/Bundle;)V')
    def on_activity_created(self, saved_instance_state):
        self.fire('activitycreated')

    @java_method('()V')
    def on_resume(self):
        self.fire('resume')

    @java_method('()V')
    def on_pause(self):
        self.fire('pause')

    @java_method('()V')
    def on_detach(self):
        self._activity = None
        self.fire('detach')

    @java_method('(Landroid/app/Activity;)V')
    def on_attach(self):
        self._activity = PyDroidApp.current_app().current_activity
        self.fire('attach', self._activity)

from .jnius import PythonJavaClass, java_method, autoclass
from .utils import Intent

import time

java_application = autoclass('com.tonyfinn.pydroid.PythonApplication').getInstance()


class PyDroidApp(PythonJavaClass):
    """
    The PyDroidApp object is used as the primary configuration of your Android
    application. It is through this object that you register activities so
    that they can be launched by the system.

    """

    __javainterfaces__ = ['com.tonyfinn.pydroid.PyDroidApp']
    __javacontext__ = 'app'

    _instance = None

    def __init__(self, package):
        """
        Create a new instance of a PyDroid application.

        :param str package: The package name for the Android application. This should
            be the name that you specify in your build.gradle and
            on the Play Store.
        """
        super(PyDroidApp, self).__init__()
        _instance = self
        self._activities = {}
        self.package = package

        #: The activity that is launched for the launcher intent. This should
        #: be the activity you want to the user to see upon first lanching the
        #: application.
        self._default_activity = None

        #: The currently running activity, if any is currently running. This
        #: is None if no activity is launched yet (such as during application
        #: startup)
        self.current_activity = None

    @classmethod
    def current_app(cls):
        """
        Returns the current application.
        :return:
        """
        return cls._instance

    def start(self):
        """
        This method should be called when application configuration is finished
        (all activities registered), and allows application initialisation to
        continue.
        """
        print('PyDroid App started')
        java_application.set_pydroid_app(self)
        while True:
            time.sleep(5)

    def register_activity(self, activity_class, activity_name=None, default=False):
        """
        Register a specific activity as being part of the current application. The
        ``activity_class`` should be a subclass of :class:`.PythonActivity`.

        An optional ``activity_name`` can be provided to name the activity. If one
        is not provided, then the name of the class will be used instead.

        The default parameter specifies whether this activity should be the first
        activity the user encounters after opening the application. This should
        only be set for one activity.

        """
        if not activity_name:
            activity_name = activity_class.__name__
        if default:
            self._default_activity = activity_name
        self._activities[activity_name] = activity_class

    @java_method('()Lcom/tonyfinn/pydroid/PythonActivity;', name='getCurrentActivity')
    def get_current_activity(self):
        """
        Accessor for :attr:`current_activity` for Java code.

        """
        return self.current_activity

    @java_method('(Lcom/tonyfinn/pydroid/ActivityProxy;Ljava/lang/String;Landroid/content/Intent;)Lcom/tonyfinn/pydroid/PythonActivity;',
                 name='launchActivity')
    def launch_activity(self, java_activity, name, intent):
        print('Launching PyActivity {}'.format(name))
        activity_class = self._activities[name]
        activity = activity_class(java_activity, self)
        activity.intent = Intent(intent)
        activity._package = self.package
        self.current_activity = activity
        return activity

    @java_method('(Lcom/tonyfinn/pydroid/ActivityProxy;Landroid/content/Intent;)Lcom/tonyfinn/pydroid/PythonActivity;',
                 name='launchDefaultActivity')
    def launch_default_activity(self, java_activity, intent):
        return self.launch_activity(java_activity, self._default_activity, intent)


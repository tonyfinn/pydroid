from .jnius import PythonJavaClass, autoclass, java_method

from .ui import View, MissingViewError, UnsupportedViewError
from .utils import Context, Bundle

androidRid = autoclass('android.R$id')


class MissingPackageError(Exception): pass


class PythonActivity(PythonJavaClass):
    __javainterfaces__ = ['com.tonyfinn.pydroid.PythonActivity']
    __javacontext__ = 'app'

    def __init__(self, java_activity, app):
        self.java_activity = java_activity
        self._context = Context(self.java_activity)
        self._intent = None
        self._app = app
        self.has_menu = False
        self.menu_name = None
        self._package = None
        self._handlers = {
            'create': [],
            'start': [],
            'pause': [],
            'resume': [],
            'activityresult': []
        }

        self.menu_handlers = {}
        super(PythonActivity, self).__init__()

    def on(self, event, handler):
        """
        Listen for an event by calling the specific handler when the event occurs.
        The event handler will be called with arguments specific to the event.
        Multiple event handlers may be registered for a single event, and in such
        a case all the event handlers will be called in the order they were
        registered.

        Currently supported events include 'create', 'pause', 'resume'
        """
        self._handlers[event].append(handler)

    @property
    def title(self):
        return self.java_activity.getActionBar().getTitle()

    @title.setter
    def title(self, title):
        self.java_activity.getActionBar().setTitle(title)

    @property
    def action_bar(self):
        return self.java_activity.getActionBar()

    def launch_own_activity(self, activity_name, **kwargs):
        """
        Launch the specified activity belonging to the current activity. The
        activity name is that provided to :meth:`.PyDroidApp.register_activity` or
        the class name if you did not provide a name.
        """
        HashMap = autoclass('java.util.HashMap')
        params = HashMap()
        for key, value in kwargs.iteritems():
            params.put(key, str(value))
        self.java_activity.launchOwnActivity(activity_name, params)

    def fire(self, event, *args):
        for handler in self._handlers[event]:
            handler(*args)

    @java_method('(Landroid/os/Bundle;)V')
    def on_create(self, saved_instance_state):
        pyBundle = Bundle(saved_instance_state)
        self.fire('create', pyBundle)

    @java_method('()V')
    def on_pause(self):
        self.fire('pause')

    @java_method('()V')
    def on_start(self):
        self.fire('start')

    @java_method('()V')
    def on_resume(self):
        self._app.current_activity = self
        self.fire('resume')

    @java_method('(Landroid/view/Menu;)Z')
    def on_create_options_menu(self, menu):
        print('Menu created: {}'.format(self.has_menu))
        if self.has_menu:
            localRId = autoclass('{}.R$menu'.format(self._package))
            elementId = getattr(localRId, self.menu_name)
            MenuInflater = autoclass('android.view.MenuInflater')
            menu_inflater = MenuInflater(self._context.native_context)
            menu_inflater.inflate(elementId, menu)
        return self.has_menu

    @java_method('(Landroid/view/MenuItem;)Z')
    def on_options_item_selected(self, item):
        if item.getItemId() in self.menu_handlers:
            return self.menu_handlers[item.getItemId()]()
        else:
            print('Unknown menu item {}'.format(item.getItemId()))
            print('Known items: {}'.format(', '.join(self.menu_handlers.keys())))
            return True

    def on_menu_item(self, item_name, handler):
        """
        Registers a event handler to be called when a specific menu item is
        clicked in either the action bar or overflow menu.

        :param str item_name: The name of the menu item, as defined in the ID in the
            menu's XML file.

        :param func handler:  The function to be called when the item is clicked.
        """
        local_r_id = autoclass('{}.R$id'.format(self._package))
        element_id = getattr(local_r_id, item_name)
        self.menu_handlers[element_id] = handler

    @java_method('(IILandroid/content/Intent;)V')
    def on_activity_result(self, requestCode, responseCode, data):
        self.fire('activityresult', requestCode, responseCode, data)

    def set_menu(self, name):
        """
        Set a menu for the current activity. There should be a corresponding
        menu definition at ``res/menu/<name>.xml``

        :param str name: The name of the XML file containing the menu definition.
        """
        self.has_menu = True
        self.menu_name = name

    def find(self, name):
        """

        Get a reference to a UI element in the current activity by its name. This
        is for UI elements declared in XML layouts, where the name should be the
        ID specified in the layout file. For example, a TextView with the UI
        id specified in the XML file as such::

            android:id="@+id/statusTextView"

        could be retrieved via a call to ``find('statusTextView')``

        :param str name: The ID of the field, normally defined in the XML layout

        :raises MissingViewError: If the view that you are attempting to load
            is not present in the current activity.
        :raises UnsupportedViewError: If the view is found, but no pydroid
            wrapper exists for that type of view.

        :return: A subclass of :class:`.ui.View` if an element is found,
            or None otherwise.
        """
        local_R_id = autoclass('{}.R$id'.format(self._package))
        element_id = getattr(local_R_id, name)
        raw_view = self.java_activity.findViewById(element_id)
        if not raw_view:
            raise MissingViewError()
        return View.get_py_view(raw_view, self._context)

    def _find(self, id):
        return self.java_activity.findViewById(id)

    @property
    def files_dir(self):
        """
        The path to the storage location on the Android device where the app
        can freely read and write files.
        """
        return self.java_activity.getFilesDir().getAbsolutePath()

    @property
    def content(self):
        """
        The root element of the Activity's UI. This will be an instance
        of :class:`.View` and can be changed by setting the value to a new view.
        """
        return self._find(androidRid.content)

    @content.setter
    def content(self, new_content):
        """
        :param View new_content: A :class:`.View` to set as the new root of the UI.
        """
        self.java_activity.getWindow().setContentView(new_content._view)

    def load_layout(self, layout):
        """
        Load a given layout file as the main view for this activity./

        :param str layout: The name of the layout file. For example, a XML file in
            ``res/layout/residence.xml`` would be loaded via ``'residence'``
        """
        if not self._package:
            raise MissingPackageError('Package needs to be set to load layouts')
        local_R_layout = autoclass('{}.R$layout'.format(self._package))
        self.java_activity.setContentView(getattr(local_R_layout, layout))

    def start_activity(self, intent):
        self.java_activity.startActivty(intent._intent)

    def start_activity_for_result(self, intent, request):
        self.java_activity.startActivityForResult(intent._intent, data_uri=request)

    @property
    def intent(self):
        return self._intent

    @intent.setter
    def intent(self, intent):
        self._intent = intent

__author__ = 'tony'


from mock import MagicMock
from .jnius import java_mock


@java_mock('android.content.Intent')
class AndroidIntentMock(MagicMock):
    ACTION_PICK = 1
    ACTION_SEND = 2
    ACTION_SENDTO = 3

    EXTRA_SUBJECT = 1
    EXTRA_TEXT = 2


class JavaActivityMock(object):
    """
    Mocks out the underlying Java Activity. You likely do not need
    """

    def __init__(self, test_app):
        self.test_app = test_app

    def launchOwnActivity(self, activity_name, **kwargs):
        self.test_app.launch_activity(**kwargs)
        pass

"""
Stub implementation of pyjnius API used for testing code without
actually loading up a full JVM which would be required to use the full API.
"""

__author__ = 'tony'
is_mock_jnius = True

from mock import MagicMock


java_mocks = {}


class JavaClass:
    pass


class JavaMock(MagicMock, JavaClass):
    __isjavamock__ = True

    def __init__(self, *args, **kwargs):
        super(JavaMock, self).__init__(*args, **kwargs)
        try:
            self.parent_mock = kwargs['_pydroid_parent_mock']
        except KeyError:
            self.parent_mock = None
        self._other_classes = {}

    def as_class(self, cls_name):
        if self.parent_mock:
            return self.parent_mock.as_class(cls_name)
        elif self._other_classes[cls_name]:
            return self._other_classes[cls_name]
        else:
            other_class = JavaMock(cls_name, _pydroid_parent_mock=self)
            self._other_classes[cls_name] = other_class


def java_method(func, name=None):
    def f(func):
        return func
    return f


def java_mock(class_name):
    """
    Decorator that registers the current class as a mcok for a Java class.

    Usage::

        @java_mock('android.content.Intent')
        class MyIntent(object):
            pass

    See :func:`.register_java_mock` for more information.

    :param class_name: The name of the Java class this class mocks.
    :return:
    """
    def f(cls):
        register_java_mock(class_name, cls)
        return cls
    return f


def register_java_mock(class_name, cls):
    """
    Registers a class as a mock for a Java class imported via autoclass.

    For example::

        class MockIntent(object):
            pass

        register_java_mock('android.content.Intent', SomeClass)

        # ...

        Intent = autoclass('android.content.Intent')
        # Intent is MockIntent

    :param class_name: The Java class name of this mock.
    :param cls: The implementation for this mock.
    :return:
    """
    java_mocks[class_name] = cls


def autoclass(class_name):
    if class_name in java_mocks:
        return java_mocks[class_name]
    else:
        new_java_mock = type(class_name, (JavaMock,), {'__javaclass__': class_name})
        java_mocks[class_name] = new_java_mock
    return java_mocks[class_name]


class PythonJavaClass(object):
    pass


def cast(name, source):
    return source.as_class(name)

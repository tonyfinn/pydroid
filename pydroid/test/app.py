__author__ = 'tony'

from .mocks import JavaActivityMock


class PyDroidTestApp(object):
    """
    A test double to use for testing PyDroid applications. Rather than running
    on an actual Android device, this allows unit tests to run locally and
    isolated from the Android API (and the need to run on a real device!)
    """
    def __init__(self, package):
        self._package = package
        self._activities = {}
        self._default_activity = None
        self.current_activity = None
        self.back_stack = []

    @property
    def current_app(self):
        return self

    def start(self):
        """
        Launch the first activity.
        """
        self._launch_activity(self._default_activity)
        self.current_activity.fire('create')
        self.current_activity.fire('start')
        self.current_activity.fire('resume')

    def _launch_activity(self, name, intent=None):
        from ..utils import Intent
        mock_java_activity = JavaActivityMock(self)
        activity_class = self._activities[name]
        activity = activity_class(mock_java_activity, self)
        if intent:
            activity.intent = Intent(intent)
        activity._package = self._package
        self.current_activity = activity

    def _finish_activity(self):
        self.current_activity.fire('pause')
        self.current_activity = self.back_stack.pop()
        self.current_activity.fire('resume')

    def register_activity(self, activity_class, activity_name=None, default=False):
        if not activity_name:
            activity_name = activity_class.__name__
        if default:
            self._default_activity = activity_name
        self._activities[activity_name] = activity_class

    def launch_own_activity(self, activity_name, **kwargs):
        self.current_activity.fire('pause')
        self.back_stack.append(self.current_activity)
        self._launch_activity(activity_name)
        self.current_activity.fire('create')
        self.current_activity.fire('start')
        self.current_activity.fire('resume')

__author__ = 'tony'

from .jnius import autoclass

from .utils import Intent

ContactsContract = autoclass('android.provider.ContactsContract')
ContactsContract.Contacts = autoclass('android.provider.ContactsContract$Contacts')


class Contact(object):
    def __init__(self, name):
        self.name = name


class Contacts(object):
    @staticmethod
    def show_contact_picker(activity, request_code):
        pick_intent = Intent(Intent.ACTION_PICK, data_uri=ContactsContract.Contacts.CONTENT_URI)
        activity.start_activity_for_result(pick_intent, request_code)

    @staticmethod
    def contact_from_uri(context, uri):
        query_fields = ["display_name"]
        cursor = context._context.getContentResolver().query(uri, query_fields)
        if cursor.getCount() == 0:
            return []
        cursor.moveToFirst()
        name = cursor.getString(0)
        return Contact(name)
